from .codex import Codex, NPCodex
from .bedtools import bedtools
from .fetch import fetch_files
from .labeler import Labeler
# from .ranges import LabeledRange, LabeledRanges

name = 'ntai'

major = 1
minor = 0
patch = 0
version = '{}.{}.{}'.format(major, minor, patch)

description = 'Nucleotide A.I.'
